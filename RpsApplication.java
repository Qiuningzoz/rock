import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application {

	//新的rpsGame object
	private RpsGame rpsGame = new RpsGame();

	public void start(Stage stage) {
		Group root = new Group();

		// scene is associated with container, dimensions
		Scene scene = new Scene(root, 750, 300);
		scene.setFill(Color.BLACK);

		// associate scene to stage and show
		stage.setTitle("Rock Paper Scissors");
		stage.setScene(scene);

		//创建按钮
		Button rockButton = new Button("Rock");
		Button scissorButton = new Button("scissors");
		Button paperButton = new Button("paper");
		//把按钮变成一行
		HBox buttons = new HBox();
		buttons.getChildren().addAll(rockButton, scissorButton, paperButton);
		//texts 变成一行
		HBox textFields = new HBox();
		TextField message = new TextField("Welcome!");
		message.setEditable(false);
		message.setPrefWidth(300);
		TextField wins = new TextField("wins:0");
		wins.setEditable(false);
		TextField losses = new TextField("losses:0");
		losses.setEditable(false);
		TextField ties = new TextField("ties:0");
		ties.setEditable(false);
		textFields.getChildren().addAll(message, wins, losses, ties);

		VBox vbox = new VBox();
		vbox.getChildren().addAll(buttons, textFields);
		root.getChildren().add(vbox);

		rockButton.setOnAction(new RpsChoice(message, wins, losses, ties, "rock", rpsGame));
		scissorButton.setOnAction(new RpsChoice(message, wins, losses, ties, "scissors", rpsGame));
		paperButton.setOnAction(new RpsChoice(message, wins, losses, ties, "paper", rpsGame));

		stage.show();
	}

	public static void main(String[] args) {
		Application.launch(args);
	}

	public class RpsChoice implements EventHandler<ActionEvent> {

		private TextField message;
		private TextField wins;
		private TextField losses;
		private TextField ties;
		private RpsGame rpsGame;
		private String choice;

		public RpsChoice(TextField message, TextField wins, TextField losses, TextField ties, String choice,
				RpsGame rpsGame) {
			this.message = message;
			this.wins = wins;
			this.losses = losses;
			this.ties = ties;
			this.rpsGame = rpsGame;
			this.choice = choice;
		}

		@Override
		public void handle(ActionEvent arg0) {
			message.setText(rpsGame.playRound(choice));;
			wins.setText("wins: " + rpsGame.getWins());
			losses.setText("losses: " + rpsGame.getLosses());
			ties.setText("ties:" + rpsGame.getTies());
		}
	}
}
