public class RpsGame {
        private int wins;
        private int losses;
        private int ties;
    
        public RpsGame() {
            wins = 0;
            losses = 0;
            ties = 0;
        }
    
        public String playRound(String choice) {
            String[] choose = new String[] { "rock", "scissors", "paper" };
            int random = (int) Math.floor(Math.random() * 3);
            String winner = "";
            if (choice.equals(choose[random])) {
                ties++;
                return "Tie Game";
            } 
            else if (choice.equals("rock"))
                if (choose[random].equals("paper")) {
                    losses++;
                    winner = "computer";
    
                } else {
                    wins++;
                    winner = "player";
                }
    
            else if (choice.equals("paper"))
                if (choose[random].equals("scissors")) {
                    losses++;
                    winner = "computer";
    
                } else {
                    wins++;
                    winner = "player";
                }
    
            else {
                if (choose[random].equals("rock")) {
                    losses++;
                    winner = "computer";
    
                } else {
                    wins++;
                    winner = "player";
                }
            }
    
            return "Computer plays " + choose[random] + " and the " + winner + " won";
        }
    
        public int getLosses() {
            return losses;
        }
    
        public int getTies() {
            return ties;
        }
    
        public int getWins() {
            return wins;
        }
    }
